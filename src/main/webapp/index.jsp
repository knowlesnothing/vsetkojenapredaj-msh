<div id="indexCenterColumn">
	<div class="featuredBox">
		<c:forEach var="item" items="${randomItems}" varStatus="count">
			<c:choose>
				<c:when test="${count.index%2 == 0}">
					<div class="featuredBoxLeft">
						<div class="itemImage">
							<a href="item-detail?item-id=${item.id}">
							<img src="images/items/${item.itemImage}" alt="item image"
								width="220px" height="220px">
							</a>
						</div>
						<h3 class="itemName">
							<a href="item-detail?item-id=${item.id}">${item.itemName}</a>
						</h3>
						<div class= "itemDescription">
							<span class="smallText">${item.itemDescription}</span>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="featuredBoxRight">
						<div class="itemImage">
							<a href="item-detail?item-id=${item.id}"> <img
								src="images/items/${item.itemImage}" alt="item image"
								width="220px" height="220px">
							</a>
						</div>
						<h3 class="itemName">
							<a href="item-detail?item-id=${item.id}">${item.itemName}</a>
						</h3>
						<div class="itemDescription">
							<span class="smallText">${item.itemDescription}</span>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</div>
</div>