
<div id="centerColumn">

	<p class="pageTitle">${selectedCategory.categoryName}</p>
	<div class="categoryContainer">
		<c:set var="categoryHasItems" value="false" scope="page" />
		<ul class="itemList cleaner">
			<c:forEach var="item" items="${itemList}">
				<c:set var="bidPlaced" value="false" scope="page" />
				<c:set var="hasBidder" value="false" scope="page" />
				<c:set var="categoryHasItems" value="true" scope="page" />
				<li class="itemList cleaner">
					<div class="itemImage">
					<a href="item-detail?item-id=${item.id}">
						<img src="images/items/${item.itemImage}" alt="item image"
							width="220px" height="220px">
					</a>
					</div> <!-- since a href uses GET, to be able to use method POST button must be a form submit button -->
					<h3 class="itemName">
						<a href="item-detail?item-id=${item.id}">${item.itemName}, </a>
						<span class="smallText">Seller ID: ${item.userId}, added: ${item.dateAdded}</span> 
					</h3>
					<div id="menuRightTop">
						<c:choose>
							<c:when test="${item.userId == user.userId}">
								<!-- delete item form -->
								<form action="delete-item" method="post">
									<input type="hidden" name="id" value="${item.id}">
									<input type="hidden" name="itemName" value="${item.itemName}">
									<button type="submit" class="submitButtonMenuRightTop" name="action" value="deleteItem" >Delete this item</button>
								</form>
								<div class="menuItem">
									<a href="edit-item?item-id=${item.id}">Edit item</a>
								</div>
							</c:when>
						</c:choose>
					</div>
					<div class="itemDescription">
						<span class="smallText">${item.itemDescription}</span>
					</div>
					<c:choose>
						<%-- TODO user logging - if this is my own item, don't show bidding options
							show bidding options only for items where item.userId != my own id --%>
						<c:when test="${item.userId != user.userId}">
							<div class="bidding">
								<!-- if there is a bid from this user for this item in bid table in db, 
                                     show "Bid placed" flag, with amount -->
								<!-- TODO fetch user id from session for logged user -->
								<!--TODO disable bidding for my own items in DB -->
								<c:forEach var="bid" items="${myMaxBidsForItemsInCategory}">
									<c:choose>
										<c:when test="${bid.itemId == item.id}">
												<div class="itemBidPlacedFlagDiv" >My bid: ${bid.bidAmount}</div>
												<c:set var="bidPlaced" value="true" scope="page" />
										</c:when>
									</c:choose>
								</c:forEach>
							</div>
							<div class="bidding">
								<!--  TODO add validation clientside and serverside for "amount must be a number" -->
								<form action="bids" method="post"  class="formFloatLeft">
									<input type="hidden" name="action" value="placeBid" />
									<input type="text" size="10" name="bidAmount" required />
									<input type="hidden" name="itemId" value="${item.id}" />
									<input type="hidden" name="userId" value="${user.userId}" />
									<button type="submit" class="submitButtonLeft">
										<c:choose>
											<c:when test="${bidPlaced}">
												Update bid
											</c:when>
											<c:otherwise>
												Place bid
											</c:otherwise>
										</c:choose>
									</button>
								</form>
								<c:choose>
									<c:when test="${bidPlaced}">
										<form action="bids" method="post" class="formFloatLeft">
											<input type="hidden" name="itemId" value="${item.id}" />
											<input type="hidden" name="userId" value="${user.userId}" />
											<button type="submit" class="submitButtonLeft" name="action" value="cancelPlacedBid">Cancel bid</button>
										</form>
									</c:when>
								</c:choose>
							</div>
						</c:when>
						<c:otherwise>
							<div class="bidding">
								<!-- if there is a bid for this item in bid table in db, 
                             show "Bid placed" flag, with highest amount,
                             otherwise show "No bids for this item of yours yet."-->
								<!-- TODO fetch user id from session for logged user -->
								<c:forEach var="bid" items="${maxBidsForMyItems}">
									<c:choose>
										<c:when test="${bid.itemId == item.id}">
												<div class="itemBidPlacedFlagDiv">Highest bid for this item: ${bid.bidAmount}</div>
												<a href="item-detail?item-id=${item.id}" class="viewBidsAnkor">View bids for this item</a>
											<c:set var="hasBidder" value="true" scope="page" />
										</c:when>
									</c:choose>
								</c:forEach>
									<c:if test="${!hasBidder}">
										<div class="bidding">
											No bids for your item yet.	
											<c:set var="hasBid" value="false" scope="page" />
										</div>
									</c:if>
							</div>
						</c:otherwise>
					</c:choose>
				</li>
			</c:forEach>
			<c:choose>
				<c:when test="${!categoryHasItems}">
					<p>There are no items in this category</p>
				</c:when>
			</c:choose>
		</ul>
	</div>
</div>

