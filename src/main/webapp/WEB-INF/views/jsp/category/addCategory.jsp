<div id="centerColumn">
	<p class="pageTitle">Add new category</p>
	<form:form action="add-category" method="post" class="addItemForm" modelAttribute="category">
		<form:label path="categoryName">Category name</form:label>
		<form:errors path="categoryName" cssClass="formError"/><br />
		<form:input path="categoryName" type="text" />
		<button type="submit" class="submitButtonLeft">Add category</button>
	</form:form>
</div>