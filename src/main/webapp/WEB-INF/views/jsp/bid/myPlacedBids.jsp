<div id="centerColumn">

	<p class="pageTitle">All my bids</p>
	<div class="categoryContainer">
		<ul class="itemList cleaner">
		<c:set var="items" value="${itemsWithMyBid}"/>
			<c:if test="${fn:length(items) == 0}">
				<p class="warning">
					<i>You are not bidding for any item.</i>
				</p>
			</c:if>
			<c:forEach var="item" items="${itemsWithMyBid}">
				<li class="itemList cleaner">
					<div class="itemImage">
						<a href="item-detail?item-id=${item.id}" >
							<img src="images/items/${item.itemImage}" alt="item image" width="220px" height="220px">
						</a>
					</div> <!-- since a href uses GET, to be able to use method POST button must be a form submit button -->
					<!-- TODO implement detail page for item -->
					<h3 class="itemName">
						<a href="item-detail?item-id=${item.id}" >${item.itemName}, </a><span class="smallText">Seller
							ID: ${item.userId}, added: ${item.dateAdded}</span>
					</h3>

					<div class="itemDescription">
						<span class="smallText">${item.itemDescription}</span>
					</div>
					<div class="bidding">

						<!-- if there is a bid from this user for this item in bid table in db, 
                                     show "Bid placed" flag, with amount -->
						<!-- TODO fetch user id from session for logged user -->
						<!--TODO disable bidding for my own items in DB -->
						<c:forEach var="bid" items="${myMaxBidsForAllItems}">
							<c:choose>
								<c:when test="${bid.itemId == item.id}">
									<form action="bids" method="post" class="formFloatLeft">
										<div class="itemBidPlacedFlagDiv" >My bid: ${bid.bidAmount}</div>
										<!--  TODO display warning about deleting not reversible, then message: Bid deleted-->
										<input type="hidden" name="itemId" value="${item.id}" />
										<input type="hidden" name="userId" value="${user.userId}" />
										<input type="text" size="10" name="bidAmount" required />
										<button type="submit" class="submitButtonLeft" name="action" value="placeBid">Update bid</button>
									</form>
									<form action="bids" method="post" class="formFloatLeft">
										<input type="hidden" name="itemId" value="${item.id}" />
										<input type="hidden" name="userId" value="${user.userId}" />
										<button type="submit" class="submitButtonLeft" name="action" value="cancelPlacedBid">Cancel bid</button>
									</form>
								</c:when>
							</c:choose>
						</c:forEach>
					</div>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>