<div id="centerColumn">
<p class="pageTitle">Messages</p>
		<c:choose>
			<c:when test="${userSwitchAttempted}">
				<p class="warning">${msg}</p>
			</c:when>
		</c:choose>
	<c:if test="${itemDeleted}">
		<p class="warning">Item ${item.itemName}, id: ${item.id} has been deleted.</p>
	</c:if>
	<c:if test="${categoryDeleteFlag}">
		<p class="warning">${msg}</p>
	</c:if>
	<c:if test="${categoryAddedFlag}">
		<p class="warning">${msg}</p>
	</c:if>

	<c:if test="${zeroCategoriesMsg != null}">
		<p class="warning">${zeroCategoriesMsg}</p>
	</c:if>
</div>