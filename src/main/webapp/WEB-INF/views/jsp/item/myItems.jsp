<%-- 
    Document   : index
    Created on : 12.4.2016, 20:13:47
    Author     : Miso2
--%>

<div id="centerColumn">

	<p class="pageTitle">My items</p>

	<div class="categoryContainer">
		<ul class="itemList cleaner">
			<c:forEach var="item" items="${myItemsList}">
			<c:set var="hasBidder" value="false" scope="page" />
				<li class="itemList cleaner">
					<div class="itemImage">
						<a href="item-detail?item-id=${item.id}"> <img
							src="images/items/${item.itemImage}" alt="item image"
							width="220px" height="220px">
						</a>
					</div>
					<h3 class="itemName">
					<a href="item-detail?item-id=${item.id}">${item.itemName}, </a>
					<span class="smallText">added: ${item.dateAdded}</span></h3>
					<div id="menuRightTop">
						<c:choose>
							<c:when test="${item.userId == user.userId}">
								<!-- delete item form -->
								<form action="delete-item" method="post">
									<input type="hidden" name="id" value="${item.id}">
									<input type="hidden" name="itemName" value="${item.itemName}">
									<button type="submit" class="submitButtonMenuRightTop">Delete this item</button>
								</form>
								<div class="menuItem">
									<a href="edit-item?item-id=${item.id}">Edit item</a>
								</div>
							</c:when>
						</c:choose>
					</div>
					<div class="itemDescription">
						<span class="smallText">${item.itemDescription}</span>
					</div>
					<div class="bidding">
						<!-- if there is a bid for this item in bid table in db, 
                           show "Bid placed" flag, with highest amount,
                           otherwise show "No bids for this item of yours yet."-->
						<!-- TODO fetch user id from session for logged user -->
						<c:forEach var="bid" items="${maxBidsForMyItems}">
							<c:choose>
								<c:when test="${bid.itemId == item.id}">
										<div class="itemBidPlacedFlagDiv">Highest bid for this item: ${bid.bidAmount}</div>
										<a href="item-detail?item-id=${item.id}" class="viewBidsAnkor">View bids for this item</a>
									<c:set var="hasBidder" value="true" scope="page" />
								</c:when>
							</c:choose>
						</c:forEach>
						<c:choose>
							<c:when test="${!hasBidder}">
							<div class="bidding">
								No bids for your item yet.
							</div>
							</c:when>
						</c:choose>
					</div>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>
