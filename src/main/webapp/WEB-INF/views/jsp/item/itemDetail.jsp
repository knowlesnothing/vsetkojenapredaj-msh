<div id="centerColumn">
	<p class="pageTitle">${item.itemName}</p>
	<div class="itemDetail">
		<div class="itemImage">
			<img src="images/items/${item.itemImage}" alt="item image" width="220px" height="220px">
		</div>
		<h3 class="itemName">
			${item.itemName}, <span class="smallText">Seller ID: ${item.userId}, added: ${item.dateAdded}, item id: ${item.id}</span>
		</h3>
		<div id="menuRightTop">
			<c:choose>
				<c:when test="${loggedInUsersItem}">
					<!-- delete item form -->
					<form action="delete-item" method="post">
						<input type="hidden" name="id" value="${item.id}">
						<input type="hidden" name="itemName" value="${item.itemName}">
						<button type="submit" class="submitButtonMenuRightTop">Delete this item</button>
					</form>
					<div class="menuItem">
						<a href="edit-item?item-id=${item.id}">Edit item</a>
					</div>
					<c:forEach var="category" items="${categoryList}">
						<c:forEach var="itemCategory" items="${itemCategories}">
							<c:if test="${itemCategory.id == category.id}">
								category id: ${category.id}<br>
							</c:if>
						</c:forEach>
					</c:forEach>
				</c:when>
			</c:choose>
		</div>

		<div class="itemDescription">
			<span class="smallText">${item.itemDescription}</span>
		</div>

		<!-- if this item belongs to logged-in user, show all bids for this item -->
		<!-- TODO fetch user id from session for logged user -->
		<c:choose>
			<c:when test="${loggedInUsersItem}">
				<div>Minimal bid: ${item.minimumBid}</div>
				<div class="bidding">
					<c:choose>
						<c:when test="${hasBidder}">
							<c:forEach var="bid" items="${allMaxBidsForMyItem}">
								<form action="bids" method="post" class="formFloatLeft">
									<input type="hidden" name="itemId" value="${bid.itemId}" />
									<input type="hidden" name="bidId" value="${bid.bidId}" />
									<div class="itemBidPlacedFlagDiv">
										User with ID: ${bid.userId} offers: ${bid.bidAmount}
									</div>
									<c:choose>
										<c:when test="${hasAcceptedBid}">
											<c:choose>
												<c:when test="${bid.bidAccepted}">
													<div class="bidAcceptedFlag">Accepted bid</div>
													<button type="submit" class="submitButtonLeft" name="action"
														value="cancelAcceptedBid">Cancel accepted bid</button>
												</c:when>
											</c:choose>
										</c:when>
										<c:otherwise>
											<button type="submit" class="submitButtonLeft" name="action"
												value="acceptBid">Accept this bid</button>
										</c:otherwise>
									</c:choose>
								</form>
								<div class="cleaner"></div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<div class="bidding">No bids for your item yet.</div>
						</c:otherwise>
					</c:choose>
				</div>
			</c:when>
			<c:otherwise>
				<c:if test="${myBidPlaced}">
					<div class="bidding">
						<div class="itemBidPlacedFlagDiv">${myMaxBidForThisItemAmount}</div>
					</div>
				</c:if>
				<div class="bidding">
					<!--  TODO add validation clientside and serverside for "amount must be a number" -->
					<form action="bids" method="post" class="formFloatLeft">
						<input type="hidden" name="action" value="placeBid" /> <input
							type="text" size="10" name="bidAmount" required /> <input
							type="hidden" name="itemId" value="${item.id}" /> <input
							type="hidden" name="userId" value="${user.userId}" />
						<button type="submit" class="submitButtonLeft">
							<c:choose>
								<c:when test="${myBidPlaced}">
									Update bid
								</c:when>
								<c:otherwise>
									Place bid
								</c:otherwise>
							</c:choose>
						</button>
					</form>
					<c:choose>
						<c:when test="${myBidPlaced}">
							<form action="bids" method="post" class="formFloatLeft">
								<input type="hidden" name="itemId" value="${item.id}" />
								<input type="hidden" name="userId" value="${user.userId}" />
								<button type="submit" class="submitButtonLeft" name="action" value="cancelPlacedBid">Cancel bid</button>
							</form>
						</c:when>
					</c:choose>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="itemGallery">
		<div class="galleryThumbnail">
			<img src="images/items/thumb.jpg" alt="thumbnail image">
		</div>
		<div class="galleryThumbnail">
			<img src="images/items/thumb.jpg" alt="thumbnail image">
		</div>
		<div class="galleryThumbnail">
			<img src="images/items/thumb.jpg" alt="thumbnail image">
		</div>
		<div class="galleryThumbnail">
			<img src="images/items/thumb.jpg" alt="thumbnail image">
		</div>
		<div class="galleryThumbnail">
			<img src="images/items/thumb.jpg" alt="thumbnail image">
		</div>
	</div>
</div>