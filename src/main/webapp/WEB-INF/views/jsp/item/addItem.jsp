<div id="centerColumn">
<p class="pageTitle">Offer item for sale</p>
	<!-- form for adding items user offers for sale to database -->
	<form action="upload-image" method="post" class="addEditForm" enctype="multipart/form-data">
		<input type="hidden" name="item-id" value="${item.id}" >
		<label for="file">Select image for your item to upload (max. 5MB)</label><br />
		<input type="text" id="uploadInput" data-readonly/>
		<div class="cleaner"></div>
		<label id="labelFileUpload" class="custom-file-input" >
			<input type="file" id="file" name="file" size="60" accept=".png,.gif,.jpeg,.jpg" />
		</label><br />
		<script>
			document.getElementById("file").onchange = function() {
				document.getElementById("uploadInput").value = document.getElementById("file").files[0].name;
			};
		</script>
		<button type="submit" class="submitButtonLeft" name="action" value="uploadImage">Upload image</button>
		<div class="cleaner"></div>
	</form>
	
	<form:form action="add-item" class="addEditForm" method="post" onsubmit="return(validate());" modelAttribute="item">
		<form:label path="itemImage" for="item_image" cssErrorClass="formError">Image</form:label>
		<form:errors path="itemImage" cssClass="formError"/><br />	
		<form:input type="text" id="item_image" path="itemImage" value="${fileName}" 
			oninvalid="this.setCustomValidity('Upload image for your item')"
			oninput="setCustomValidity('')" required="required" data-readonly="data-readonly"/><br />
		<div class="cleaner"></div>
		
		<form:label path="itemName" for="item_name" cssErrorClass="formError">Item name</form:label>
	    <form:errors path="itemName" cssClass="formError"/><br />
	    <form:input type="text" id="item_name" path="itemName" required="required" />
		<div class="cleaner"></div>
	    
	    <form:label path="itemDescription" for="item_description" cssErrorClass="formError">Description</form:label>
	    <form:errors path="itemDescription" cssClass="formError"/><br />
	    <form:textarea cols="80" rows="8" id="item_description" path="itemDescription" required="required" /><br />
	    <form:label path="minimumBid" for="minimum_bid" cssErrorClass="formError">Minimum bid</form:label><br />
	    <form:input type="text" id="minimum_bid" path="minimumBid" required="required" /><br />
	    <div class="cleaner"></div>
	    
	    <form:label path="visible" for="visible" cssErrorClass="formError">Make item:</form:label><br />
	    <form:radiobutton path="visible" value="0" /> Hidden
	    <form:radiobutton path="visible" value="1" /> Visible
	    <br /><br />
	    <fieldset id="checkboxGroupAddItem">
			<c:forEach var="category" items="${categoryList}">
				<c:set var="itemInThisCategory" value="false" scope="page" />
				<c:forEach var="itemCategory" items="${itemCategories}">
					<c:choose>
						<c:when test="${itemCategory.id == category.id}">
							<c:set var="itemInThisCategory" value="true" scope="page" />
						</c:when>
					</c:choose>
				</c:forEach>
				<label for="${category.categoryName}">${category.categoryName}</label>
				<c:choose>
					<c:when test="${itemInThisCategory}">
						<input type="checkbox" id="${category.categoryName}" checked="checked" name="catCheckboxes" value="${category.id}">
					</c:when>
					<c:otherwise>
						<input type="checkbox" id="${category.categoryName}" name="catCheckboxes" value="${category.id}">
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</fieldset>
	    <!-- TODO fetch user id from session for logged user -->
	    <form:input type="hidden" path="userId" value="${user.userId}" />
	    <button type="submit" class="submitButtonLeft">Add item</button>
	</form:form>
</div>

<div class="modalAlert ui-state-error" id="dialog-message"
	title="Select category for item">
	<p>
		<span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
		Please select category / categories for your item.
	</p>
</div>


<script>
$(function() {
$("#file").change(function () {
	    if(fileExtValidate(this)) {
	    	 if(fileSizeValidate(this)) {
	    	 	showImg(this);
	    	 }	 
	    }    
    });

// File extension validation, Add more extensions you want to allow
var validExt = ".png, .gif, .jpeg, .jpg";
function fileExtValidate(fdata) {
 var filePath = fdata.value;
 var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
 var pos = validExt.indexOf(getFileExt);
 if(pos < 0) {
	 alert("Please upload valid file - allowed extensions are: .png, .gif, .jpeg, .jpg");
 	return false;
  } else {
  	return true;
  }
}

// file size validation
// size in kb
var maxSize = '5120';
function fileSizeValidate(fdata) {
	 if (fdata.files && fdata.files[0]) {
                var fsize = fdata.files[0].size/1024;
                if(fsize > maxSize) {
                	 alert('Maximum file size (5MB) exceed, This file size is: ' + Math.round(fsize/1024)
                			 + "MB. Upload will fail.");                	 
                	 return false;
                } else {
                	return true;
                }
     }
 }	
});
</script>
