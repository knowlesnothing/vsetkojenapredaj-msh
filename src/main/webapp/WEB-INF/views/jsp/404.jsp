<div id="centerColumn">
	<p class="pageTitle">HTTP Status 404</p>

	<p class="warning">HTTP Status 404 - Sorry, page ${path} not found.</p>
</div>