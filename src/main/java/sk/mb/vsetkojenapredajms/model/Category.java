
package sk.mb.vsetkojenapredajms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

@Entity
@Table(name="category")
@Component
public class Category implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private short id;
	
	@NotBlank(message="- Category name can not be left empty")
	@Column(name="category_name", nullable=false)
    private String categoryName;
    
	@ManyToMany
	@JoinTable(name="category_has_item",
		joinColumns=@JoinColumn(name="category_id"),
		inverseJoinColumns=@JoinColumn(name="item_id"))
    private List<Item> categoryItems = new ArrayList<>();

    public Category() {
    }

    public Category(String name) {
        this.categoryName = name;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String name) {
        this.categoryName = name;
    }
    
    public List<Item> getCategoryItems() {
		return categoryItems;
	}

	public void setCategoryItems(List<Item> categoryItems) {
		this.categoryItems = categoryItems;
	}

	public String toString() {
		return "category name: " + categoryName + ", category id: " + id;
    }

}
