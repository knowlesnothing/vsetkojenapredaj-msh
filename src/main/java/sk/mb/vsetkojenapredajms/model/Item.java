package sk.mb.vsetkojenapredajms.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="item")
public class Item {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable = false)
	private Long id;
	
	@NotBlank(message="Item name can not be left empty")
	@Size(max = 45, min = 5, message="- Item name should be between 5 - 45 characters long.")
	@Column(name="item_name", nullable = false)
	private String itemName;
	
	@NotBlank
	@Size(max = 500, min = 10, message="- Item description should be between 10 - 500 characters long.")
	@Column(name="item_description", nullable = false)
	private String itemDescription;
	
	@NotBlank(message="Image name can not be left empty - upload an image for your item.")
	@Size(max = 45, min = 5, message="- Image name should be between 5 - 45 characters long.")
	@Column(name="item_image", nullable = false)
	private String itemImage;
	
	@Column(name="date_added", nullable = true)
	private String dateAdded;
	
	@Column(name="visible", nullable = false)
	private int visible;
	
	@Column(name="minimum_bid", nullable = false)
	private double minimumBid;
	
	@Column(name="user_id", nullable = false)
	private long userId;
	
    @ManyToMany(mappedBy = "categoryItems")
	private List<Category> itemCategories = new ArrayList<>();
    
    @OneToMany(mappedBy="item", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Bid> bidsForItem = new ArrayList<>();

	public Item() {

	}

	public Item(long id, String itemName, String itemDescription, String itemImage, int visible, double minimumBid) {
		this.id = id;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemImage = itemImage;
		this.visible = visible;
		this.minimumBid = minimumBid;
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public int getVisible() {
		return visible;
	}

	public void setVisible(int visible) {
		this.visible = visible;
	}

	public double getMinimumBid() {
		return minimumBid;
	}

	public void setMinimumBid(double minimumBid) {
		this.minimumBid = minimumBid;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "Item [id=" + id + ", itemName=" + itemName + ", itemDescription=" + itemDescription + ", itemImage="
				+ itemImage + ", dateAdded=" + dateAdded + ", visible=" + visible + ", minimumBid=" + minimumBid
				+ ", userId=" + userId + ", itemCategories=" + itemCategories + ", bidsForItem=" + bidsForItem + "]";
	}

	
	public List<Category> getItemCategories() {
		return itemCategories;
	}

	public void setItemCategories(List<Category> itemCategories) {
		this.itemCategories = itemCategories;
	}
	
	public List<Bid> getBidsForItem() {
		return bidsForItem;
	}

	public void setBidsForItem(List<Bid> bidsForItem) {
		this.bidsForItem = bidsForItem;
	}

	public void placeBid(Bid bid) {
		bidsForItem.add(bid);
		bid.setItem(this);
	}

	public void cancelBid(List<Bid> bidsToRemove) {
		bidsForItem.removeAll(bidsToRemove);
	}

	public void acceptBid(Bid bidToAccept) {
		int i = bidsForItem.indexOf(bidToAccept);
		bidsForItem.get(i).setBidAccepted(true);
	}

	public void cancelAcceptedBid(Bid bidAcceptedToCancel) {
		int i = bidsForItem.indexOf(bidAcceptedToCancel);
		bidsForItem.get(i).setBidAccepted(false);
	}

	// No transient object is equal to any other transient or persisted object
	// for MySQL generated id, for JPA entities without a natural business key
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return getId() != null && Objects.equals(getId(), item.getId());
    }
 
    // I know... but this is what Vlad Mihalcea says is best for GenerationType.IDENTITY
    // https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
    @Override
    public int hashCode() {
        return 31;
    }
}
