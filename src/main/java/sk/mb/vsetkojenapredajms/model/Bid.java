package sk.mb.vsetkojenapredajms.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="bid")
public class Bid {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private long bidId;

	@NotBlank
	@Column(name="bid_amount", nullable=false)
	private double bidAmount;
	
	@Column(name="item_id", nullable=false, insertable=false, updatable=false)
	private long itemId;
	
	@Column(name="user_id", nullable=false)
	private long userId;
	
	@Column(name="date_bid_placed", nullable=true, updatable=false)
	private Date dateBidPlaced;
	
	@Column(name="bid_accepted", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean bidAccepted;
	
	@ManyToOne
	private Item item;
	
	public Bid() {
		
	}
	
	public Bid(long itemId, long userId) {
		this.itemId = itemId;
		this.userId = userId;
	}
	
	public Bid(double bidAmount, long itemId, long userId) {
		this.bidAmount = bidAmount;
		this.itemId = itemId;
		this.userId = userId;
	}

	public long getBidId() {
		return bidId;
	}
	
	public void setBidId(long bidId) {
		this.bidId = bidId;
	}

	public double getBidAmount() {
		return bidAmount;
	}

	public void setBidAmount(double bidAmount) {
		this.bidAmount = bidAmount;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Date getDateBidPlaced() {
		return dateBidPlaced;
	}

	public void setDateBidPlaced(Date dateBidPlaced) {
		this.dateBidPlaced = dateBidPlaced;
	}

	public boolean getBidAccepted() {
		return bidAccepted;
	}

	public void setBidAccepted(boolean bidAccepted) {
		this.bidAccepted = bidAccepted;
	}

	public Item getItem() {
		return item;
	}
	
	public void setItem(Item item) {
		this.item = item;
	}
	
	@Override
	public String toString() {
		return "Bid [bidId=" + bidId + ", bidAmount=" + bidAmount + ", itemId=" + itemId + ", userId=" + userId
				+ ", dateBidPlaced=" + dateBidPlaced + ", bidAccepted=" + bidAccepted + "]";
	}
}
