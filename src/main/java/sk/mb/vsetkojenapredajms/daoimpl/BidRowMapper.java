package sk.mb.vsetkojenapredajms.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sk.mb.vsetkojenapredajms.model.Bid;

public class BidRowMapper implements RowMapper<Bid> {

	@Override
	public Bid mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Bid bid = new Bid();
//		bid.setBidId(resultSet.getLong("id")); // TODO is this id used somewhere?
		bid.setBidAmount(resultSet.getDouble("bid_amount"));
		bid.setDateBidPlaced(resultSet.getDate("date_bid_placed"));
		bid.setUserId(resultSet.getLong("user_id"));
		bid.setBidAccepted(resultSet.getBoolean("bid_accepted"));
		bid.setItemId(resultSet.getLong("item_id"));
	
		return bid;
	}
}
