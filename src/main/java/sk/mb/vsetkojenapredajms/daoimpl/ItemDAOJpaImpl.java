package sk.mb.vsetkojenapredajms.daoimpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sk.mb.vsetkojenapredajms.dao.ItemDAOJpa;
import sk.mb.vsetkojenapredajms.model.Item;

@Repository
public class ItemDAOJpaImpl implements ItemDAOJpa {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ItemDAOJpaImpl.class);
	
	private static final String CATEGORY_ID = "categoryId";
	private static final String ITEM_ID = "itemId";

	private NamedParameterJdbcTemplate namedParamJdbcTemplate;

	public ItemDAOJpaImpl(DataSource dataSource) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
    @PersistenceContext
    private EntityManager em;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Item> getRandomItems() {
		// for random items it is enough to fetch some 50 recently added items
		// TODO change to 50
		final int ITEM_NUM = 16;

		// create an list of existing items
		Query query = em.createQuery("SELECT i FROM Item i WHERE i.id > ((SELECT max(i.id) FROM Item) - " + ITEM_NUM + ")");
		List<Item> itemsSelection = query.getResultList();
		List<Item> randomItems = new ArrayList<>();
		// get 4 random items
		LOGGER.info("itemsSelection.size(): " + itemsSelection.size());

		while(randomItems.size() != 4) {
			Item item = itemsSelection.get(new Random().nextInt(itemsSelection.size()));
			if(!(randomItems.contains(item))) {
				randomItems.add(item);
			} 
		}	
		return randomItems;
	}
	
    @Override
	@Transactional(readOnly = true)
	public Item getItemDetails(long itemId) throws DataAccessException {
		TypedQuery<Item> query = this.em.createQuery("SELECT i FROM Item i JOIN FETCH i.itemCategories WHERE i.id = :itemId", Item.class);
		query.setParameter("itemId", itemId);
		return query.getSingleResult();
    }
    
	@Override
	@SuppressWarnings("unchecked")
	public List<Item> getMyItems(long userId) {
		LOGGER.info("in getMyItems");
		return em.createQuery("SELECT i FROM Item i WHERE i.userId = :userId")
				.setParameter("userId", userId)
				.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Item> getItemsWithMyBid(long userId) {
		
		return em.createQuery("SELECT i FROM Item i JOIN FETCH i.bidsForItem b "
				+ "WHERE (b.itemId, b.bidAmount, b.dateBidPlaced) IN (SELECT b.itemId, MAX(b.bidAmount), MAX(b.dateBidPlaced) FROM Bid b WHERE b.userId = :userId GROUP BY b.itemId)")
				.setParameter("userId", userId)
				.getResultList();
	}
	
	@Override
	@Transactional
	public void saveEditedItem(Item item, String[] categoryIds) {
		em.merge(item);
		
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(item);
		String deleteItemCategoriesSQL = "DELETE FROM category_has_item WHERE item_id = :id;";
		namedParamJdbcTemplate.update(deleteItemCategoriesSQL, beanParams);

			insertItemCategoriesOnEdit(item, categoryIds);
	}

	private void insertItemCategoriesOnEdit(Item item, String[] categoryIds) {
		if(categoryIds != null) {
			for (String categoryId : categoryIds) {
				Map<String, Object> parameters = new HashMap<>();
				long itemId = item.getId();
				parameters.put(CATEGORY_ID, categoryId);
				parameters.put(ITEM_ID, itemId);
				
				SqlParameterSource sqlParams = new MapSqlParameterSource(parameters);
				
				String insertCategoriesSQL = "INSERT INTO category_has_item (category_id, item_id) VALUES (:categoryId, :itemId)";
				namedParamJdbcTemplate.update(insertCategoriesSQL, sqlParams);
			}
		} else {
			throw new IllegalArgumentException("Number of categories for item must not be null");
		}
	}
	
	@Override
	@Transactional
	public void deleteItem(long itemId) {
		Item item = em.find(Item.class, itemId);
		em.remove(item);}
	
	@Override
	@Transactional
	public long addItem(Item item, String[] categoryIds) {
		em.persist(item);
		
		insertItemCategoriesOnAdd(categoryIds);
		return item.getId();
	}

	private void insertItemCategoriesOnAdd(String[] categoryIds) {
		String insertCategoriesSQL = "INSERT INTO category_has_item (item_id, category_id)"
									+ "VALUES (LAST_INSERT_ID(), :categoryId)";
		if(categoryIds != null) {
			for (String categoryId : categoryIds) {
				
				SqlParameterSource sqlParams = new MapSqlParameterSource(CATEGORY_ID, categoryId);
				
				namedParamJdbcTemplate.update(insertCategoriesSQL, sqlParams);
			}
		} else {
			throw new IllegalArgumentException("Number of categories for item must not be null");
		}
	}
}