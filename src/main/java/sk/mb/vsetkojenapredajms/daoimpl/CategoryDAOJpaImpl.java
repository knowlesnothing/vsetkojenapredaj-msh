package sk.mb.vsetkojenapredajms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sk.mb.vsetkojenapredajms.dao.CategoryDAOJpa;
import sk.mb.vsetkojenapredajms.model.Category;

@Repository
public class CategoryDAOJpaImpl implements CategoryDAOJpa {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDAOJpaImpl.class);

    @PersistenceContext
    private EntityManager em;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Category> getAllCategories() {
		return em.createQuery("SELECT c FROM Category c").getResultList();
	}
	
	@Override
	public Category getSelectedCategory(short categoryId) {
		return (Category) this.em.createQuery("SELECT c FROM Category c "
				+ "LEFT JOIN FETCH c.categoryItems WHERE c.id = :categoryId")
				.setParameter("categoryId", categoryId)
				.getSingleResult();
	}

	@Override
	@Transactional
	public Category addCategory(Category category) {
		em.persist(category);
		em.flush();
		LOGGER.info("category added, id: " + category.getId());
		return category;
	}

	@Override
	@Transactional
	public void deleteCategory(Category categoryToDelete) {
		Category category = em.find(Category.class, categoryToDelete.getId());
		if(category != null) {
			em.remove(category);
		}
	}
}
