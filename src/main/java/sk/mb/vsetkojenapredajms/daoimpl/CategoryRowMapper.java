package sk.mb.vsetkojenapredajms.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sk.mb.vsetkojenapredajms.model.Category;

public class CategoryRowMapper implements RowMapper<Category> {

	@Override
	public Category mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Category category = new Category();
		category.setId(resultSet.getShort("id"));
		category.setCategoryName(resultSet.getString("category_name"));
	
		return category;
	}



}
