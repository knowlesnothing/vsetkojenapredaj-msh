package sk.mb.vsetkojenapredajms.daoimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import sk.mb.vsetkojenapredajms.dao.BidDAO;
import sk.mb.vsetkojenapredajms.model.Bid;

@Repository
public class BidDAOImpl implements BidDAO {

	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	private static final String ITEM_ID = "itemId";
	private static final String USER_ID = "userId";

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Override
	public boolean placeBid(Bid bid) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(bid);
		String sql = "INSERT INTO bid (bid_amount, item_id, user_id) values (:bidAmount, :itemId, :userId)";

		return namedParamJdbcTemplate.update(sql, beanParams) == 1;
	}

	@Override
	public boolean cancelPlacedBid(Bid bid) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(bid);
		String sql = "DELETE FROM bid WHERE item_id = :itemId AND user_id = :userId";
		
		return namedParamJdbcTemplate.update(sql, beanParams) == 1;
	}
	
	@Override
	public boolean acceptBid(Bid bid) {
		SqlParameterSource beanParamsForAccept = new BeanPropertySqlParameterSource(bid);
		String sqlSetAcceptedBid = "UPDATE bid SET bid_accepted = 1 WHERE bid.id = :bidId;";
		int bidAccepted = namedParamJdbcTemplate.update(sqlSetAcceptedBid, beanParamsForAccept);
		return bidAccepted == 1;		
	}
	
	@Override
	public void cancelAcceptedBid(Bid bid) {
		SqlParameterSource beanParamsForNullify = new BeanPropertySqlParameterSource(bid);
		String sqlNullifyBidsForItem = "UPDATE bid SET bid_accepted = 0 WHERE item_id = :itemId;";
		namedParamJdbcTemplate.update(sqlNullifyBidsForItem, beanParamsForNullify);
	}

	//not used in the JPA version
	@Override
	public List<Bid> getMyMaxBidsForAllItems(long userId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource(USER_ID, userId);
		String sql = "SELECT id, item_id, MAX(bid_amount) as bid_amount, date_bid_placed, bid_accepted, user_id "
				+ "FROM bid "
				+ "WHERE bid_amount IS NOT NULL AND user_id = :userId "
				+ "GROUP BY item_id;";
		return namedParamJdbcTemplate.query(sql, sqlParams, new BidRowMapper());
	}
	
	// ok for only_full_group_by
	@Override
	public Bid getMyMaxBidForThisItem(long userId, long itemId) {
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put(USER_ID, userId);
		paramMap.put(ITEM_ID, itemId);
		Bid myMaxBidForThisItem = null;
		
		SqlParameterSource sqlParams = new MapSqlParameterSource(paramMap);
		
		String sql = "SELECT id,  bid_amount, bid.item_id, date_bid_placed, user_id, bid_accepted "
				+ "FROM bid "
				+ "WHERE (bid.bid_amount, date_bid_placed) IN "
				+"(SELECT MAX(bid_amount), MAX(date_bid_placed) FROM bid WHERE bid.item_id = :itemId AND bid.user_id = :userId GROUP BY bid.user_id);";
		//TODO use a method returning single row, such that allows an empty result set
		List<Bid> myMaxBidForThisItemCanBeNull = namedParamJdbcTemplate.query(sql, sqlParams, new BidRowMapper());
		if(!myMaxBidForThisItemCanBeNull.isEmpty()) {
			myMaxBidForThisItem = myMaxBidForThisItemCanBeNull.get(0);
		}		
		return myMaxBidForThisItem;
	}
	
	// ok for only_full_group_by
	@Override
	public List<Bid> getAllMaxBidsForMyItem(long itemId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource(ITEM_ID, itemId);		
		String sql = "SELECT * FROM bid WHERE (user_id, bid_amount, date_bid_placed, item_id) "
				+ "IN ("
					+ "SELECT user_id, MAX(bid_amount), MAX(date_bid_placed), item_id "
					+ "FROM bid "
					+ "WHERE item_id = :itemId "
					+ "GROUP BY user_id"
					+ ");";

		return namedParamJdbcTemplate.query(sql, sqlParams, new BidRowMapper());
	}

	// ok for only_full_group_by
	@Override
	public List<Bid> getMyMaxBidsForItemsInCategory(long userId, long categoryId) {
		Map<String, Long> parameters = new HashMap<>();
		parameters.put(USER_ID, userId);
		parameters.put("categoryId", categoryId);

		SqlParameterSource sqlParams = new MapSqlParameterSource(parameters);
		String sql = "SELECT * FROM bid "
				+ "WHERE (bid.item_id, bid.bid_amount, date_bid_placed) "
				+ "IN (SELECT bid.item_id, MAX(bid_amount), MAX(date_bid_placed) "
				+ "FROM bid JOIN category_has_item ON bid.item_id = category_has_item.item_id "
				+ "WHERE user_id = :userId AND category_has_item.category_id = :categoryId "
				+ "GROUP BY bid.item_id);";
				return namedParamJdbcTemplate.query(sql, sqlParams, new BidRowMapper());
	}

	// ok for only_full_group_by
	@Override
	public List<Bid> getMaxBidsForMyItems(long userId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource(USER_ID, userId);
		String sql = "SELECT bid.* FROM item JOIN bid ON item.id = bid.item_id "
				+ "WHERE (bid.item_id, bid.bid_amount, bid.date_bid_placed) "
				+ "IN (SELECT bid.item_id, MAX(bid.bid_amount), MAX(bid.date_bid_placed) FROM bid GROUP BY bid.item_id) "
				+ "AND item.user_id = :userId;";

		return namedParamJdbcTemplate.query(sql, sqlParams, new BidRowMapper());
	}
}
