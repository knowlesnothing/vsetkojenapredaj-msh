package sk.mb.vsetkojenapredajms.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sk.mb.vsetkojenapredajms.dao.BidDAOJpa;
import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Item;

@Repository
public class BidDAOJpaImpl implements BidDAOJpa {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BidDAOJpaImpl.class);

    @PersistenceContext
    private EntityManager em;

	@Override
	@Transactional
	public void placeBid(Bid bid) {
		Item item = findItem(bid);
		item.placeBid(bid);
	}

	@Override
	@Transactional
	public void cancelPlacedBid(Bid bid) {
		Item item = findItem(bid);
		
		List<Bid> bidsToRemove = new ArrayList<>();
		for (Bid bidToRemove : item.getBidsForItem()) {
			if (bidToRemove.getUserId() == bid.getUserId())
				bidsToRemove.add(bidToRemove);
		}
		item.cancelBid(bidsToRemove);
	}

	@Override
	@Transactional
	public void acceptBid(Bid bid) {
		Item item = findItem(bid);
		Bid bidToAccept = findBid(bid, item);
		item.acceptBid(bidToAccept);
	}

	@Override
	@Transactional
	public void cancelAcceptedBid(Bid bid) {
		Item item = findItem(bid);
		Bid bidAcceptedToCancel = findBid(bid, item);
		item.cancelAcceptedBid(bidAcceptedToCancel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Bid getMyMaxBidForThisItem(long userId, long itemId) {
		Bid myMaxBidForThisItem = null;
		
		Query query = em.createQuery("SELECT b FROM Bid b WHERE (b.bidAmount, b.dateBidPlaced) "
				+ "IN (SELECT MAX(b.bidAmount), MAX(b.dateBidPlaced) FROM b WHERE b.itemId = :itemId AND b.userId = :userId GROUP BY b.userId)")
				.setParameter("itemId", itemId)
				.setParameter("userId", userId);
		
		List<Bid> myMaxBidForThisItemCanBeNull = query.getResultList();
		if(!myMaxBidForThisItemCanBeNull.isEmpty()) {
			myMaxBidForThisItem = myMaxBidForThisItemCanBeNull.get(0);
		}		
		return myMaxBidForThisItem;
	}
	
	// ok for only_full_group_by
	@SuppressWarnings("unchecked")
	@Override
	public List<Bid> getAllMaxBidsForMyItem(long itemId) {
		return em.createQuery("SELECT b FROM Bid b WHERE (b.userId, b.bidAmount, b.dateBidPlaced, b.itemId) "
				+ "IN ("
				+ "SELECT b.userId, MAX(b.bidAmount), MAX(b.dateBidPlaced), b.itemId "
				+ "FROM Bid b "
				+ "WHERE b.itemId = :itemId "
				+ "GROUP BY b.userId)")
				.setParameter("itemId", itemId)
				.getResultList();
	}

	@Override
	@Transactional
	public List<Bid> getMyMaxBidsForItemsInCategory(List<Item> itemList, long userId) {
		List<Item> itemsFromOthers = new ArrayList<>();
		for (Item item : itemList) {
			Item persistedItem = em.find(Item.class, item.getId());
			if(persistedItem.getUserId() != userId) {
				itemsFromOthers.add(persistedItem);
			}
		}

		List<Bid> myMaxBidsForItemsInCategory = new ArrayList<>();
		for (Item item : itemsFromOthers) {
			List<Bid> myBidsForItem = item.getBidsForItem();

			double myMaxBidAmountForItemInCategory = 0.0;
			Bid myMaxBidForItemInCategory = null;
			for (Bid bid : myBidsForItem) {
				double currentBidAmount = bid.getBidAmount();
				if (currentBidAmount > myMaxBidAmountForItemInCategory) {
					myMaxBidAmountForItemInCategory = currentBidAmount;
					myMaxBidForItemInCategory = bid;
				}
			}
			myMaxBidsForItemsInCategory.add(myMaxBidForItemInCategory);
		}
		return myMaxBidsForItemsInCategory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Bid> getMaxBidsForMyItems(long userId) {
		return em.createQuery("SELECT b FROM Item i JOIN FETCH Bid b ON i.id = b.itemId "
				+ "WHERE i.userId = :userId "
				+ "AND (b.itemId, b.bidAmount, b.dateBidPlaced) IN "
				+ "(SELECT b.itemId, MAX(b.bidAmount), MAX(b.dateBidPlaced) FROM b GROUP BY b.itemId)")
				.setParameter("userId", userId)
				.getResultList();
	}

	private Item findItem(Bid bid) {
		return em.find(Item.class, bid.getItemId());
	}

	private Bid findBid(Bid bid, Item item) {
		Bid bidToAccept = null;
		for (Bid bidForItem : item.getBidsForItem()) {
			if (bidForItem.getBidId() == bid.getBidId()) {
				bidToAccept = bidForItem;
				break;
			}
		}
		return bidToAccept;
	}
}
