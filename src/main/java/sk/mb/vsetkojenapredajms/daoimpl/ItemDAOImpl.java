package sk.mb.vsetkojenapredajms.daoimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sk.mb.vsetkojenapredajms.dao.ItemDAO;
import sk.mb.vsetkojenapredajms.model.Item;

@Repository
public class ItemDAOImpl implements ItemDAO {
	
	private static final String CATEGORY_ID = "categoryId";
	private static final String ITEM_ID = "itemId";

	private NamedParameterJdbcTemplate namedParamJdbcTemplate;

	public ItemDAOImpl(DataSource dataSource) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Override
	public List<Item> getRandomItems() {
		String sql = "SELECT * FROM item ORDER BY MD5(CONCAT(id, CURRENT_TIMESTAMP)) LIMIT 4;";

		return namedParamJdbcTemplate.query(sql, new ItemRowMapper());
	}
	

	@Override
	public List<Item> getAllItemsInCategory(long categoryId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource(CATEGORY_ID, categoryId);

		String sql = "SELECT item_name, item_description, item_image, id, date_added, visible, minimum_bid, user_id "
				+ "FROM item JOIN category_has_item "
				+ "ON item.id = category_has_item.item_id "
				+ "WHERE category_has_item.category_id = :categoryId";

		return namedParamJdbcTemplate.query(sql, sqlParams, new ItemRowMapper());
	}

	@Override
	public Item getItemDetails(long itemId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource(ITEM_ID, itemId);
		String sql = "SELECT * FROM item WHERE id = :itemId;";
		return namedParamJdbcTemplate.queryForObject(sql, sqlParams, new ItemRowMapper());
	}

	@Override
	public List<Item> getMyItems(long userId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource("userId", userId);
		String sql = "SELECT id, item_name, item_description, item_image, date_added, visible, minimum_bid, user_id FROM item WHERE user_id = :userId";

		return namedParamJdbcTemplate.query(sql, sqlParams, new ItemRowMapper());
	}

	@Override
	public List<Item> getItemsWithMyBid(long userId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource("userId", userId);

		String sql = "SELECT item.* "
				+ "FROM item JOIN bid ON item.id = bid.item_id "
				+ "WHERE (item.id, bid.bid_amount) IN "
				+ "(SELECT bid.item_id, max(bid.bid_amount) "
				+ "FROM bid "
				+ "WHERE bid.user_id = :userId "
				+ "GROUP BY bid.item_id);";
		
		return namedParamJdbcTemplate.query(sql, sqlParams, new ItemRowMapper());
	}

	@Override
	@Transactional
	public void saveEditedItem(Item item, String[] categoryIds) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(item);
		String updateItemSQL = "UPDATE item "
				+ "SET item_name = :itemName, item_description = :itemDescription, item_image = :itemImage, visible = :visible, minimum_bid = :minimumBid "
				+ "WHERE id = :id;";

		namedParamJdbcTemplate.update(updateItemSQL, beanParams);
		
		String deleteItemCategoriesSQL = "DELETE FROM category_has_item WHERE item_id = :id;";
		namedParamJdbcTemplate.update(deleteItemCategoriesSQL, beanParams);

			insertItemCategoriesOnEdit(item, categoryIds);
	}

	private void insertItemCategoriesOnEdit(Item item, String[] categoryIds) {
		if(categoryIds != null) {
			for (String categoryId : categoryIds) {
				Map<String, Object> parameters = new HashMap<>();
				long itemId = item.getId();
				parameters.put(CATEGORY_ID, categoryId);
				parameters.put(ITEM_ID, itemId);
				
				SqlParameterSource sqlParams = new MapSqlParameterSource(parameters);
				
				String insertCategoriesSQL = "INSERT INTO category_has_item (category_id, item_id) VALUES (:categoryId, :itemId)";
				namedParamJdbcTemplate.update(insertCategoriesSQL, sqlParams);
			}
		} else {
			throw new IllegalArgumentException("Number of categories for item must not be null");
		}
			
	}
	
	@Override
	public boolean deleteItem(long itemId) {
		SqlParameterSource sqlParams = new MapSqlParameterSource(ITEM_ID, itemId);
		String sql = "DELETE FROM item WHERE id = :itemId";
		return namedParamJdbcTemplate.update(sql, sqlParams) == 1;
	}
	
	@Override
	@Transactional
	public long addItem(Item item, String[] categoryIds) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(item);
		String insertItemSQL = "INSERT INTO item (item_name, item_description, item_image, minimum_bid, visible, user_id) "
				+ "VALUES (:itemName, :itemDescription, :itemImage, :minimumBid, :visible, :userId)";

		namedParamJdbcTemplate.update(insertItemSQL, beanParams);

		insertItemCategoriesOnAdd(categoryIds);

		SqlParameterSource sqlParams = new MapSqlParameterSource(); // empty, no params in the sql
		String lastInsertedItemIdSQL = "SELECT * FROM item WHERE id = LAST_INSERT_ID()";
		Item lastInsertedItem = namedParamJdbcTemplate.queryForObject(lastInsertedItemIdSQL, sqlParams, new ItemRowMapper());
		return lastInsertedItem.getId();
	}

	private void insertItemCategoriesOnAdd(String[] categoryIds) {
		String insertCategoriesSQL = "INSERT INTO category_has_item (item_id, category_id)"
									+ "VALUES (LAST_INSERT_ID(), :categoryId)";
		if(categoryIds != null) {
			for (String categoryId : categoryIds) {
				
				SqlParameterSource sqlParams = new MapSqlParameterSource(CATEGORY_ID, categoryId);
				
				namedParamJdbcTemplate.update(insertCategoriesSQL, sqlParams);
			}
		} else {
			throw new IllegalArgumentException("Number of categories for item must not be null");
		}
	}
}