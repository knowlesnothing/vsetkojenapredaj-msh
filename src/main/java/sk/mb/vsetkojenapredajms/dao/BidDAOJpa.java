package sk.mb.vsetkojenapredajms.dao;

import java.util.List;

import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Item;

public interface BidDAOJpa {
	/**
	 * Return logged-in user's highest bid for item with passed in itemId
	 * 
	 * @param loggedInUserId
	 * @param itemId
	 * @return Bid
	 */
	Bid getMyMaxBidForThisItem(long userId, long itemId);
	
	/**
	 * Return list of all bids for item displayed in detailed view
	 * 
	 * @param itemId
	 * @return List<Bid>
	 */
	List<Bid> getAllMaxBidsForMyItem(long itemId);
	
	/**
	 * For every item in selected category get highest bid for logged user id
	 * 
	 * If there is a bid from logged in user for this item in bid table in db,
	 * "Bid placed" flag is shown, with amount
	 * 
	 * @param List<Item> itemList
	 * @param long userId
	 * @return List<Bid>
	 */
	List<Bid> getMyMaxBidsForItemsInCategory (List<Item> itemList, long userId);
	
	/**
	 * Return list of bids for my items
	 * 
	 * @param userId
	 * @return List<Bid>
	 */
	List<Bid> getMaxBidsForMyItems(long userId);
	
	/**
	 * place bid for item,
	 * TODO allows placing multiple bids, ONLY if new is higher than old
	 * 
	 * @param Bid
	 */
	void placeBid(Bid bid);
	
	/**
	 * Delete all bids for this item for logged-in user
	 * 
	 * @param Bid
	 */
	void cancelPlacedBid(Bid bid);
	
	/**
	 * Set bid with this bid id as accepted.
	 * Sets all other bids for this item to 0, that is not accepted.
	 * TODO Buyer will see that his bid was accepted, too. 
	 *
	 * @param bid
	 * @return boolean
	 */
	void acceptBid(Bid bid);
	
	/**
	 * Set previously accepted bid to not accepted,
	 * sets bid_accepted flag in db to 0 in all bids for this item
	 * 
	 * @param long itemId
	 */
	void cancelAcceptedBid(Bid bid);
}
