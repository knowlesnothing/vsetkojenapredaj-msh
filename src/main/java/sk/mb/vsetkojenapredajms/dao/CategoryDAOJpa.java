package sk.mb.vsetkojenapredajms.dao;

import java.util.List;

import javax.persistence.Query;

import sk.mb.vsetkojenapredajms.model.Category;
import sk.mb.vsetkojenapredajms.model.Item;

/**
 * @author Miso2
 *
 */
public interface CategoryDAOJpa {

	/**
	 * Return a list of all categories, for category list / buttons / links in left menu
	 * 
	 * @return List<Category> - all categories
	 */
	List<Category> getAllCategories();
	
	/**
	 * Return name of category identified by number in query string stored
	 * in request link - category?id
	 * - For showing selected category name in left menu
	 * 
	 * - For setting selected categories when edit item form must be shown again after error
	 * 
	 * @param categoryId
	 * @return Category - selected category
	 */
	Category getSelectedCategory(short categoryId);

	
	/**
	 * Add new category
	 * 
	 * @param category
	 */
	Category addCategory(Category category);

	
	/**
	 * Delete category
	 * 
	 * @param categoryToDelete
	 */
	void deleteCategory(Category categoryToDelete);    
}
