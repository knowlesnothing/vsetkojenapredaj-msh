package sk.mb.vsetkojenapredajms.dao;

import java.util.List;

import sk.mb.vsetkojenapredajms.model.Category;

public interface CategoryDAO {

	/**
	 * Return a list of all categories, for category list / buttons / links in left menu
	 * 
	 * @return List<Category> - all categories
	 */
	List<Category> getAllCategories();
	
	/**
	 * Return name of category identified by number in query string stored
	 * in request link - category?id
	 * - for showing selected category name in left menu
	 * - for setting selected categories when edit item form must be shown again after error
	 * 
	 * @param categoryId
	 * @return Category - selected category
	 */
	Category getSelectedCategory(long categoryId);
	
	
	/**
	 * Return list of all categories this item is in
	 * 
	 * @param itemId
	 * @return List<Category>
	 */
	List<Category> getItemCategories(long itemId);
}
