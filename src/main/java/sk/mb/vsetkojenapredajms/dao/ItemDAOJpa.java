package sk.mb.vsetkojenapredajms.dao;
import java.util.List;

import org.springframework.dao.DataAccessException;

import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Item;


/**
 * Repository class for <code>Item</code> domain objects 
 * 
 * @author Miso2
 */
public interface ItemDAOJpa {
	
	/**
	 * Return list of randomly chosen items for display as featured on the index page
	 * @return List<Item>
	 */
	List<Item> getRandomItems();	
	
    /**
     * Retrieve an <code>Item</code> from the data store by id - for displaying detailed view
     *
     * @param itemId the id to search for
     * @return the <code>Item</code> if found
     * @throws org.springframework.dao.DataRetrievalFailureException if not found
     */
    Item getItemDetails(long itemId) throws DataAccessException;
		
	/**
	 * Return list of items for logged-in user
	 * 
	 * @param userId
	 * @return List<Item>
	 */
	List<Item> getMyItems(long userId);
	
	/**
	 * Return all items for which logged-in user has placed bids
	 * 
	 * @param userId
	 * @return List<Item>
	 */
	List<Item> getItemsWithMyBid(long userId);
	
	/**
	 * transaction!!! TODO - check first, if something about this item was changed,
	 * if true, update all item's details in the database from the form THEN
	 * insert into category_has_item records for item category or categories,
	 * if item belongs to more categories
	 * 
	 * @param item
	 * @param categoryIds
	 */
	void saveEditedItem(Item item, String[] categoryIds);

	/**
	 * Delete item with passed in itemId
	 * 
	 * @param itemId
	 */
	void deleteItem(long itemId);
	
	/**
	 * transaction!!! - insert all item's details into database from the form THEN
	 * insert into category_has_item records for item category or categories,
	 * if item belongs to more categories
	 * 
	 * @param item
	 * @param categoryIds
	 * @return 
	 */
	long addItem(Item item, String[] categoryIds);
}
