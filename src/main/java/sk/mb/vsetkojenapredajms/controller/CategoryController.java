package sk.mb.vsetkojenapredajms.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sk.mb.vsetkojenapredajms.dao.BidDAO;
import sk.mb.vsetkojenapredajms.dao.BidDAOJpa;
import sk.mb.vsetkojenapredajms.dao.CategoryDAOJpa;
import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Category;
import sk.mb.vsetkojenapredajms.model.Item;
import sk.mb.vsetkojenapredajms.model.User;

@Controller
public class CategoryController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private CategoryDAOJpa categoryDAOJpa;

	@Autowired
	private BidDAOJpa bidDAOJpa;

	@Autowired
	private ServletContext servletContext;
	
	@PostConstruct
	public void initializeCategories() {
		getAllCategoriesForLeftMenu();
	}
	
	public long getUserId() {
		User user = (User) servletContext.getAttribute("user");
		return user.getUserId();
	}

	@RequestMapping("/category")
	public String showSelectedCategory(@RequestParam("category-id") Short categoryId,
										Model model) {
		getAllCategoriesForLeftMenu();
		
		Category selectedCategory = null;
		
		if(categoryId != null) {
			try {
				selectedCategory = categoryDAOJpa.getSelectedCategory(categoryId);
				model.addAttribute("selectedCategory", selectedCategory);
			} catch (EmptyResultDataAccessException e) {
				LOGGER.error("Error in CategoryController, showSelectedCategory(), case (\"category\"): ", e);
			} 
		}
		if (selectedCategory != null) {
			
			long userId = getUserId();
			
			List<Item> itemList = selectedCategory.getCategoryItems();
			model.addAttribute("itemList", itemList);
	
			List<Bid> myMaxBidsForItemsInCategory = bidDAOJpa.getMyMaxBidsForItemsInCategory(itemList, userId);
			model.addAttribute("myMaxBidsForItemsInCategory", myMaxBidsForItemsInCategory);
	
			List<Bid> maxBidsForMyItems = bidDAOJpa.getMaxBidsForMyItems(userId);
			model.addAttribute("maxBidsForMyItems", maxBidsForMyItems);
			
			return "category/category";
		}
		return "404";
	}
	
	@GetMapping("/add-category")
	public String showAddCategoryForm(Model model) {
		model.addAttribute("category", new Category());
		return "category/addCategory";
	}
	
	@PostMapping("/add-category")
	public String addCategory(@Valid Category category, BindingResult result, Model model) {
		LOGGER.info("category name: " + category.getCategoryName());
		if(result.hasErrors()) {
			return "category/addCategory";
		}
		
		boolean categoryAddedFlag = true;
		model.addAttribute("categoryAddedFlag", categoryAddedFlag);
		
		Category addedCategory = categoryDAOJpa.addCategory(category);
		getAllCategoriesForLeftMenu();
		String msg = "Category id: " + addedCategory.getId() + ", name: " + addedCategory.getCategoryName() + " has been added.";
		model.addAttribute("msg", msg);
		return "message";
	}
	
	@PostMapping("/delete-category")
	public String deleteCategory(Category category, Model model) {
		Category categoryToDelete = categoryDAOJpa.getSelectedCategory(category.getId());
		LOGGER.info("category: " + categoryToDelete.getCategoryName() + ", items: " + categoryToDelete.getCategoryItems());
		String deletedCategoryName = categoryToDelete.getCategoryName();
		
		boolean categoryDeleteFlag = true;
		model.addAttribute("categoryDeleteFlag", categoryDeleteFlag);
		
		String msg = null;
		if(categoryToDelete.getCategoryItems().size() == 0) {
			categoryDAOJpa.deleteCategory(categoryToDelete);
			getAllCategoriesForLeftMenu();
			msg = "Category " + deletedCategoryName + " has been deleted.";
		} else {
			msg = "Category is not empty, can not delete.";
		}
		model.addAttribute("msg", msg);
		return "message";
	}

	public void getAllCategoriesForLeftMenu() {
		List<Category> categoryList = categoryDAOJpa.getAllCategories();
		servletContext.setAttribute("categoryList", categoryList);
	}
	
}
