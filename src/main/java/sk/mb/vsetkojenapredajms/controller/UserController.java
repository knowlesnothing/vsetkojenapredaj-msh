package sk.mb.vsetkojenapredajms.controller;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import sk.mb.vsetkojenapredajms.model.User;

@Controller
public class UserController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private ServletContext servletContext;

	private User user;

	private long userId;

	@PostConstruct
	public void initializeUser() {
		LOGGER.info("in UserController, initializeUser()");
		//TODO user login
		user = new User(2);
		servletContext.setAttribute("user", user);
		userId = user.getUserId();
	}
	
	@GetMapping("/login")
	public String handleGetRequests() {
			return "user/login";
	}
	
	@RequestMapping("/switch-user")
	public String switchUser(@RequestParam(name="userId", required=false) Long userId, Model model) {
		LOGGER.info("in switchUser()");

		boolean userSwitchAttempted = true;
		model.addAttribute("userSwitchAttempted", userSwitchAttempted);
		long currentUserId = user.getUserId();
		String msg;
		if (userId != null) {
			if (userId > 0 && userId <= 3) {
				user = new User(userId);
				servletContext.setAttribute("user", user);

				this.userId = user.getUserId();
				msg = "User ID changed to: " + this.userId;

			} else {
				msg = "Unknown user ID: " + userId + ". Choose user ID 1, 2 or 3.<br>" + "<br>Current user ID: "
						+ currentUserId;
			}
		} else {
			msg = "Invalid user choice, choose user ID 1, 2 or 3.<br>" + "<br>Current user ID: " + currentUserId;
		}
		model.addAttribute("msg", msg);
		return "message";
	}
}
