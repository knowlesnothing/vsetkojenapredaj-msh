package sk.mb.vsetkojenapredajms.controller;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sk.mb.vsetkojenapredajms.dao.BidDAO;
import sk.mb.vsetkojenapredajms.dao.BidDAOJpa;
import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.User;

@Controller
public class BidController {

	@Autowired
	private BidDAOJpa bidDAOJpa;

	@Autowired
	private ServletContext servletContext;

	public long getUserId() {
		User user = (User) servletContext.getAttribute("user");
		return user.getUserId();
	}

	@PostMapping("/bids")
	public String bidHandler(Bid bid,
							@RequestParam(name = "action") String action,
							Model model) {
		if (action != null) {
			switch (action) {
				
				case "placeBid":
					bidDAOJpa.placeBid(bid);
					break;
	
				case "cancelPlacedBid":
					bidDAOJpa.cancelPlacedBid(bid);
					break;
					
				case "acceptBid":
					bidDAOJpa.acceptBid(bid);
					break;
					
				case "cancelAcceptedBid":
					bidDAOJpa.cancelAcceptedBid(bid);
					break;
	
				default:
					return "404";
				}
		}
		return "redirect:/item-detail?item-id=" + bid.getItemId();
	}
}
