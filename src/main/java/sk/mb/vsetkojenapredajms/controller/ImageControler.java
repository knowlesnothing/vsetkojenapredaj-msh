package sk.mb.vsetkojenapredajms.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sk.mb.vsetkojenapredajms.dao.CategoryDAO;
import sk.mb.vsetkojenapredajms.dao.ItemDAOJpa;
import sk.mb.vsetkojenapredajms.model.Category;
import sk.mb.vsetkojenapredajms.model.Item;

@Controller
public class ImageControler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageControler.class);
	
	@Autowired
	private ServletContext servletContext;

	@Autowired
	private ItemDAOJpa itemDAOJpa;

	@Autowired
	private CategoryDAO categoryDAO;

	@RequestMapping("upload-image")
	public String uploadImage(RedirectAttributes redirectAttributes,
								@RequestParam("file") Part file,
								@RequestParam("item-id") Long itemId,
								Model model) throws IOException {
		
	LOGGER.info("in uploadImage(), item-id: " + itemId);
    
	final String IMAGE_SAVE_DIR = "resources/images/items";    
    
	// gets absolute path of the web application
	String appPath = servletContext.getRealPath("");

	// constructs path of the directory to save uploaded file
	String savePath = appPath + IMAGE_SAVE_DIR;

	File fileSaveDir = new File(savePath);
	// creates the save directory if it does not exist
	if (!fileSaveDir.exists()) {
		fileSaveDir.mkdir();
	}
	String fileName = extractFileName(file);
	
	// refines the fileName in case it is an absolute path
	fileName = new File(fileName).getName();
	file.write(savePath + File.separator + fileName);

	redirectAttributes.addFlashAttribute("fileName", fileName);
	model.addAttribute("fileName", fileName);				
	
	if (itemId != null) {
			setItemDetailsForEdit(itemId, model);
			return "redirect:/edit-item?item-id=" + itemId;
		} else {
			return "redirect:/add-item";
		}

	}
	
	/**
	 * Extracts file name from HTTP header content-disposition
	 */
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf('=') + 2, s.length()-1);
			}
		}
		return "";
	}
//	TODO decouple upload image form from edit item form somehow
	private void setItemDetailsForEdit(Long itemId, Model model) {
		LOGGER.info("in ImageController.setItemDetailsForEdit()");
		Item item = itemDAOJpa.getItemDetails(itemId);
		model.addAttribute("item", item);
		
		List<Category> itemCategories = item.getItemCategories();
		model.addAttribute("itemCategories", itemCategories);
	}
}
