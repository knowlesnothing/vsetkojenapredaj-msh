package sk.mb.vsetkojenapredajms.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import sk.mb.vsetkojenapredajms.dao.BidDAOJpa;
import sk.mb.vsetkojenapredajms.dao.CategoryDAOJpa;
import sk.mb.vsetkojenapredajms.dao.ItemDAOJpa;
import sk.mb.vsetkojenapredajms.model.Bid;
import sk.mb.vsetkojenapredajms.model.Category;
import sk.mb.vsetkojenapredajms.model.Item;
import sk.mb.vsetkojenapredajms.model.User;

@Controller
public class ItemController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);

	private final String MESSAGE = "message";
	
	@Autowired
	private ServletContext servletContext;

	@Autowired
	ItemDAOJpa itemDAOJpa;
	
	@Autowired
	BidDAOJpa bidDAOJpa;
	
	@Autowired
	CategoryDAOJpa categoryDAOJpa;
	
	@PostConstruct
	public void setRandomItemsForIndex() {
		List<Item> randomItems = itemDAOJpa.getRandomItems();
		servletContext.setAttribute("randomItems", randomItems);	
	}
	
	@RequestMapping("/vsetkojenapredajms")
	public String callIndex() {
		setRandomItemsForIndex();
		return "redirect:/";
	}
	
	public long getUserId() {
		User user = (User) servletContext.getAttribute("user");
		return user.getUserId();
	}
	
	@RequestMapping("/add-item")
	public String showAddItemForm(Model model) {
		LOGGER.info("in showAddItemForm()");
		model.addAttribute("item", new Item());
		return "item/addItem";
	}
	
	@RequestMapping(value="/add-item", method=RequestMethod.POST)
	public String addItem(@Valid Item item,
							BindingResult result,
							@RequestParam(name = "catCheckboxes") String[] categoryIds,
							Model model) {
		
		LOGGER.info("in addItem()");
	
		long lastInsertedItemId;
		
		if(result.hasErrors()) {
			LOGGER.info("result.hasErrors()");
			List<Category> itemCategories = new ArrayList<Category>();
			for (String categoryId : categoryIds) {
				itemCategories.add(categoryDAOJpa.getSelectedCategory(Short.parseShort(categoryId)));
			}
			
			model.addAttribute("itemCategories", itemCategories);
			return "item/addItem";
		}

		if (categoryIds != null) {
			lastInsertedItemId = itemDAOJpa.addItem(item, categoryIds);
		} else {
			String zeroCategoriesMsg = "Please select at least one category for this item.";
			model.addAttribute("zeroCategoriesMsg", zeroCategoriesMsg);
			return MESSAGE;
		}
		return "redirect:/item-detail?item-id=" + lastInsertedItemId;
	}
	
	@RequestMapping(value="/delete-item", method=RequestMethod.POST)
	public String deleteItem(Item item, Model model) {
		itemDAOJpa.deleteItem(item.getId());
		model.addAttribute("item", item);
		model.addAttribute("itemDeleted", true);
		return MESSAGE;
	}
	
	@GetMapping("/item-detail")
	public String showItemDetail(@RequestParam(name = "item-id", required=false) Long itemId,
									Model model) {

		User user = (User) servletContext.getAttribute("user");
		long userId = user.getUserId();
		
		Item item = null;
		
		if(itemId != null) {
			item = itemDAOJpa.getItemDetails(itemId);
		}
		
		if (item != null) {
			model.addAttribute(item);

			if (userId == item.getUserId()) {
				boolean loggedInUsersItem = true;
				model.addAttribute("loggedInUsersItem",loggedInUsersItem);
				
				List<Bid> allMaxBidsForMyItem = bidDAOJpa.getAllMaxBidsForMyItem(itemId);
				
				if(!allMaxBidsForMyItem.isEmpty()) {
					model.addAttribute("allMaxBidsForMyItem", allMaxBidsForMyItem);
					
					boolean hasBidder = true;
					model.addAttribute("hasBidder", hasBidder);
					
					boolean hasAcceptedBid = checkAcceptedBid(allMaxBidsForMyItem);
					model.addAttribute("hasAcceptedBid", hasAcceptedBid);
				}

				List<Category> itemCategories = item.getItemCategories();
				model.addAttribute("itemCategories", itemCategories);
				
				
			} else {
				Bid myMaxBidForThisItem = bidDAOJpa.getMyMaxBidForThisItem(userId, itemId);
				
				if (myMaxBidForThisItem != null) {
					
					boolean myBidPlaced = true;
					model.addAttribute("myBidPlaced", myBidPlaced);
					
					String myMaxBidForThisItemAmount = "My bid: " + myMaxBidForThisItem.getBidAmount();
					model.addAttribute("myMaxBidForThisItemAmount", myMaxBidForThisItemAmount);
				} 
			}
			return "item/itemDetail";
		}
		return "404";
	}
	
	private boolean checkAcceptedBid(List<Bid> allMaxBidsForMyItem) {
		for (Bid bid : allMaxBidsForMyItem) {
			if(bid.getBidAccepted()) {
				return true;
			}
		}
		return false;
	}
	
		@RequestMapping("/edit-item")
		public String showItemEditForm(@RequestParam(name="item-id") Long itemId,
										Model model) {

		setItemDetailsForEdit(itemId, model);
		return "item/editItem";
	}
	
	private void setItemDetailsForEdit(Long itemId, Model model) {
		LOGGER.info("in ItemController.setItemDetailsForEdit()");
		Item item = itemDAOJpa.getItemDetails(itemId);
		model.addAttribute("item", item);
		
		List<Category> itemCategories = item.getItemCategories();
		model.addAttribute("itemCategories", itemCategories);
	}
	
	@RequestMapping(value="save-edited-item", method=RequestMethod.POST)
	public String saveEditedItem(@Valid Item item,
								BindingResult result,
								@RequestParam(name="catCheckboxes") String[] categoryIds,
								Model model) {
		
		if(result.hasErrors()) {
			LOGGER.info("result.hasErrors()");
			List<Category> itemCategories = new ArrayList<Category>();
			for (String categoryId : categoryIds) {
				itemCategories.add(categoryDAOJpa.getSelectedCategory(Short.parseShort(categoryId)));
			}
			
			model.addAttribute("itemCategories", itemCategories);
			return "/item/editItem";
		}
		
		if (categoryIds != null) {
			itemDAOJpa.saveEditedItem(item, categoryIds);
		} else {
			String zeroCategoriesMsg = "Please select at least one category for this item.";
			model.addAttribute("zeroCategoriesMsg", zeroCategoriesMsg);
			return MESSAGE;
		}
		return "redirect:/item-detail?item-id=" + item.getId();
	}

	
	@RequestMapping("my-items")
	public String showMyItems(Model model) {
		LOGGER.info("in showMyItems()");	
//	TODO user login
		long userId = getUserId();
		
		List<Item> myItemsList = itemDAOJpa.getMyItems(userId);
		model.addAttribute("myItemsList", myItemsList);
		
		List<Bid> maxBidsForMyItems = bidDAOJpa.getMaxBidsForMyItems(userId);
		model.addAttribute("maxBidsForMyItems", maxBidsForMyItems);
		return "item/myItems";
	}
	
	@GetMapping("/my-placed-bids")
	public String showMyPlacedBids(Model model) {
		LOGGER.info("in showMyPlacedBids()");
		long userId = getUserId();
		
		List<Item> itemsWithMyBid = itemDAOJpa.getItemsWithMyBid(userId);
		model.addAttribute("itemsWithMyBid", itemsWithMyBid);
		
		List<Bid> myMaxBidsForAllItems = new ArrayList<>();
		for (Item item : itemsWithMyBid) {
			myMaxBidsForAllItems.add(item.getBidsForItem().get(0));
		}

		model.addAttribute("myMaxBidsForAllItems", myMaxBidsForAllItems);

		return "bid/myPlacedBids";
	}
}
