package sk.mb.vsetkojenapredajms.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
		authBuilder.inMemoryAuthentication()
			.withUser("demo")
			.password("demopass2017")
			.authorities("USER")
		.and()
			.withUser("miso")
			.password("misopass2109")
			.authorities("USER", "ADMIN");
	}
		
    @Override
    public void configure(WebSecurity security)
    {
        security.ignoring().antMatchers("/css/images/design/*", "/css/*", "/resources/**");
    }
    
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
			
		httpSecurity.authorizeRequests()
		.antMatchers("/edit-item", "/item-detail", "/add-item", "/category", "/my-items", "/my-placed-bids", "/message", "/logout", "/switch-user").hasAuthority("USER")
//		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/login").failureUrl("/login?error")
		.usernameParameter("username")
		.passwordParameter("password")
		.permitAll()
	.and().logout()
		.logoutUrl("/logout")
		.logoutSuccessUrl("/login?loggedOut")
		.invalidateHttpSession(true).deleteCookies("JSESSIONID")
		.permitAll()
	.and()
		.csrf().disable();
		
	}

	
}
